from .cart import Cart
from shop.models import InfoModel, Category, Product



def cart(request):
	cat = Category.objects.filter(parent__isnull=True)
	cart = Cart(request)
	return {
	'cart': cart, 
	'info': InfoModel.objects.get(pk=1),
	'categories': cat,
	}