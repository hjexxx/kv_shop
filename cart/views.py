from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from shop.models import Product
from .cart import Cart
from .forms import CartAddProductForm
from django.template.loader import render_to_string
from django.http import HttpResponse,JsonResponse

@require_POST
def CartAdd(request, product_id):
	if request.is_ajax:
		cart = Cart(request)
		product = get_object_or_404(Product, id=product_id)
		form = CartAddProductForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			print(cd)
			cart.add(product=product, quantity=cd['quantity'],
									  update_quantity=cd['update'])
			data = {
		        'total_items': len(cart),
		        'total_price': cart.get_total_price(),
		        'product_name': product.name,
		        'product_image': product.image.url,

		        
	    	}
			# return HttpResponse({'total': , 'count': cart.__iter__()})
			return JsonResponse(data)
	return HttpResponse('lox')

def CartRemove(request, product_id):
	cart = Cart(request)
	product = get_object_or_404(Product, id=product_id)
	cart.remove(product)
	data = {
	'status': 'ok',
	'product_id': str(product.id),
	'items': len(cart),
	'total_price': cart.get_total_price(),
	}
	return JsonResponse(data)


def CartDetail(request):
	cart = Cart(request)
	if cart:
		for item in cart:
			if item:
				item['update_quantity_form'] = CartAddProductForm(
												initial={
													'quantity': item['quantity'],
													'update': True
												})
		return render(request, 'cart/detail.html', {'cart': cart})
	else:
		return render(request, 'cart/not.html')