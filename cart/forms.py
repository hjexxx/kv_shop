# -*- coding: utf-8 -*-
from django import forms

PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 11)]
class CartAddProductForm(forms.Form):
	quantity = forms.IntegerField(initial=1, widget=forms.TextInput( attrs = {'id': 'quanity-input', 'readonly': 'readonly','style': 'width: 20px; border: none; text-align:center;'} ))
	update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)