# -*- coding: utf-8 -*-
from django import forms

class ContactForm(forms.Form):
	phone = forms.CharField(required=True, max_length=30,label='Телефон')

class ContactEmailForm(forms.Form):
	email = forms.CharField(required=True, max_length=40, widget=forms.TextInput(attrs={'placeholder': 'Введите Email'}))