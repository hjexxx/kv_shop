# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from .models import Cat, Product, InfoModel, InfoModel1, Rent, SliderItem, Review, ContactEmail
from cart.forms import CartAddProductForm
from django.template import Context, Template, RequestContext
from django.db.models import Q
from cart.cart import ProductHistory
from django.template.loader import render_to_string
from django.http import HttpResponse
from shop.forms import ContactForm, ContactEmailForm
from django.http import HttpResponse,JsonResponse
from django.core.mail import EmailMessage


from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# def ProductList(request, Cat_slug=None):
#     return render(request, 'shop/product/product-list.html', {
#         'category': Cat_slug,
#     })

def ProductList(request, Cat_slug=None):
    cat = None
    product_list = Product.objects.all().order_by('-created')
    page = request.GET.get('page', 1)
    if Cat_slug and Cat_slug != 'all':
        cat = get_object_or_404(Cat, slug=Cat_slug)
        product_list = product_list.filter(category=cat)

    paginator = Paginator(product_list, 40)

    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    try:
        tree = cat.get_ancestors()
    except:
        tree = False
    try:
        tree_ch = cat.get_children()
    except:
        tree_ch = False
    
    return render(request, 'shop/product/product-list.html', {
        'tree': tree,
        'tree_ch': tree_ch,
        'category': cat,
        'products': products,
    })

def contactPhone(request):
    if request.is_ajax:
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            data = {
                'status': 'ok',
            }
            message = 'Телефон: ' + cd['phone']
            email = EmailMessage('Заявка на звонок', message, to=[InfoModel.objects.get(pk=1).email])
            email.send()
            return JsonResponse(data)
    return HttpResponse('ERORR')

def contactEmail(request):
    if request.is_ajax:
        form = ContactEmailForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            data = {
                'status': 'ok'
            }
            email_to = cd['email']
            message = InfoModel1.objects.get(pk=1).email_text
            ContactEmail.objects.create(email=email_to)
            email = EmailMessage('Подписка оформлена', message, to=[email_to])
            email.send()
            return JsonResponse(data)
    return HttpResponse('ERROR')


def search(request):
    question = request.GET.get('q')
    if question is not None:
        products = Product.objects.filter(Q(name__icontains=question)|Q(description__icontains=question))
        if products:
            return render(request, 'shop/product/search-success.html', {'products':products})
    return render(request, 'shop/product/search-not-fount.html')
    

def MainPage(request):
    new_products = Product.objects.filter(is_new=True)
    sale_products = Product.objects.filter(old_price__isnull=False)
    contact = ContactForm()
    emailForm = ContactEmailForm()
    cart_product_form = CartAddProductForm()
    slider_items = SliderItem.objects.all()
    history = ProductHistory(request)
    history_ids = list(history)[-4:]
    products_history = Product.objects.filter(id__in=history_ids).reverse()
    return render(request, 'shop/product/main-page.html', 
        {
        'new_products':new_products,
        'sale_products': sale_products,
        'cart_product_form': cart_product_form,
        'seo': InfoModel1.objects.get(pk=1).seo,
        'seo_title': InfoModel1.objects.get(pk=1).seo_title,
        'contact': contact,
        'emailForm': emailForm,
        'slider_items': slider_items,
        'reviews': Review.objects.all(),
        'pr_history': products_history,
        })

def ProductDetail(request, cat_slug, slug):
    history = ProductHistory(request)
    product = get_object_or_404(Product, id=int(slug.split('--')[1]), slug=slug.split('--')[0])
    history_ids = list(history)[-4:]
    history.add(product)
    products_history = Product.objects.filter(id__in=history_ids).reverse()
    cart_product_form = CartAddProductForm()
    try:
        tree = product.category.get_ancestors()
    except:
        tree = False


    
    return render(request, 'shop/product/detail.html', 
    	{'product': product,
        'tree': tree,
    	'cart_product_form': cart_product_form,
        'pr_history': products_history}) 



def contacts(request):
    template = 'shop/contacts.html'

    info = InfoModel1.objects.get(id=1).info
    rekv = InfoModel1.objects.get(id=1).rekv

    ctx = {
        'info1': info,
        'rekv': rekv,
    }
    return render(request, template, ctx)

def rent(request):
    rent = Rent.objects.all()
    return render(request, 'shop/rent.html', {'rents': rent})

def service(request):
    service = InfoModel1.objects.get(id=1).service
    return render(request, 'shop/service.html', {'service': service})

def shipping(request):
    return render(request, 'shop/shipping.html')

def garant(request):
    return render(request, 'shop/garant.html')

def about(request):
    return render(request, 'shop/about.html')

def vozvrat(request):
    return render(request, 'shop/vozvrat.html')
