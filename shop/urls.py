from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^contacts/$', views.contacts, name='contacts'),
    url(r'^search/$', views.search, name='search'),
    url(r'^rent/$', views.rent, name='rent'),
    url(r'^service/$', views.service, name='service'),
    url(r'^contact-phone/$', views.contactPhone, name='contact-phone'),
    url(r'^contact-email/$', views.contactEmail),
    url(r'^shipping/$', views.shipping, name='shipping'),
    url(r'^garant/$', views.garant, name='garant'),
    url(r'^about/$', views.about, name='about'),
    url(r'^vozvrat/$', views.vozvrat, name='vozvrat'),

    url(r'^(?P<Cat_slug>[-\w]+)/$', views.ProductList, name='ProductListByCategory'),
    url(r'^(?P<cat_slug>[-\w]+)/(?P<slug>[-\w]+)/$', views.ProductDetail, name='ProductDetail'),

    url(r'^all/$', views.ProductList, name='ProductList'),
    url(r'^$', views.MainPage, name='MainPage'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
