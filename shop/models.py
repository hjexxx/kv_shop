# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from embed_video.fields import EmbedVideoField
from tinymce.models import HTMLField
from mptt.models import MPTTModel, TreeForeignKey

class ContactEmail(models.Model):
	email = models.CharField(max_length=40, verbose_name="Email")
	def __unicode__(self):
			return self.email


class SliderItem(models.Model):
	image = models.ImageField(upload_to='products/', verbose_name="Изображение")
	title = models.CharField(max_length=80, verbose_name='Текст')
	button = models.BooleanField(verbose_name='Кнопка')
	button_text = models.CharField(max_length=20, verbose_name='Текст на кнопке', null=True, blank=True)
	button_link = models.CharField(max_length=40, verbose_name='Ссылка', null=True, blank=True)
	def __unicode__(self):
    		return self.title
	class Meta:
		verbose_name = 'Слайд'
		verbose_name_plural = 'Слайды'
	
class Rent(models.Model):
	title = models.CharField(max_length=20, verbose_name="Заголовок")
	title_t = models.CharField(max_length=200, verbose_name="Подзаголовок", blank=True, null=True)
	image = models.ImageField(upload_to='products/', verbose_name="Изображение")
	bez_zal = HTMLField(blank=True, verbose_name='Без залога')
	s_zal = HTMLField(blank=True, verbose_name='С залогом')
	class Meta:
		ordering = ['title']
		verbose_name = 'Блок аренды'
		verbose_name_plural = 'Аренда'

class InfoModel1(models.Model):
	info = HTMLField(blank=True, verbose_name='Контакты')
	rekv = HTMLField(blank=True, verbose_name="Реквизиты")
	rent = HTMLField(blank=True, verbose_name="Аренда")
	service = HTMLField(blank=True, verbose_name="Сервисный центр")
	seo_title = models.CharField(max_length=100, blank=True, default="Рекламный блок", verbose_name='Заголовок рекламного блока')
	seo = HTMLField(blank=True, verbose_name='Рекламный блок')
	email_text = HTMLField(blank=True, verbose_name='Текст подписки')
	class Meta:
		verbose_name = 'Информация'
		verbose_name_plural = 'Информация'

class InfoModel(models.Model):
	dostavka = HTMLField(blank=True, verbose_name='Курьерская доставка в подвале')
	phone = HTMLField(blank=True, verbose_name='Телефон')
	email = models.EmailField(blank=True, verbose_name='Email')
	address = models.CharField(max_length=100, blank=True, verbose_name='Адрес')
	about = models.TextField(max_length=200, blank=True, null=True)
	class Meta:
		verbose_name = 'Контакты'
		verbose_name_plural = 'Контакты'

class Review(models.Model):
	name = models.CharField(max_length=50, verbose_name='Имя')
	text = models.TextField(max_length=200, verbose_name='Текст отзыва')
	class Meta:
		verbose_name = 'Отзывы'
		verbose_name_plural = 'Отзыв'

	def __unicode__(self):
    		return 'Отзыв от ' + self.name

class Category(models.Model):
	name = models.CharField(max_length=200, db_index=True, verbose_name='Имя категории')
	slug = models.SlugField(max_length=200, db_index=True, unique=True)
	parent = models.ForeignKey('self', blank=True, null=True, related_name='child')

	has_child = models.BooleanField(default=False, verbose_name='Выпадающее меню')

	def get_absolute_url(self):
		return reverse('shop:ProductListByCategory', args=[self.slug])
	class Meta:
		ordering = ['name']
		verbose_name = 'Категория'
		verbose_name_plural = 'Категории'

	def __unicode__(self):
		return self.name

class Cat(MPTTModel):
	name = models.CharField(max_length=50, unique=True)
	parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
	slug = models.SlugField(max_length=50, db_index=True, unique=True)
	icon = models.ImageField(upload_to='products/', blank=True, verbose_name="Иконка", null=True)
	image = models.ImageField(upload_to='products/', blank=True, verbose_name="Изображение", null=True)
	def get_absolute_url(self):
		return reverse('shop:ProductListByCategory', args=[self.slug])
	class Meta:
		verbose_name_plural = 'Kатегории'    		
	class MPTTMeta:
		order_insertion_by = ['name']

	def __unicode__(self):
		return self.name

class Product(models.Model):
	category = models.ForeignKey(Cat, related_name='products', verbose_name="Категория", null=True, blank=True)
	name = models.CharField(max_length=54, db_index=True, verbose_name="Название (макс. длина 54 символов)")
	slug = models.SlugField(max_length=200, db_index=True)
	image = models.ImageField(upload_to='products/', blank=True, verbose_name="Изображение товара1", null=True)
	image2 = models.ImageField(upload_to='products/', blank=True, verbose_name="Изображение товара2", null=True)
	image3 = models.ImageField(upload_to='products/', blank=True, verbose_name="Изображение товара3", null=True)
	image4 = models.ImageField(upload_to='products/', blank=True, verbose_name="Изображение товара4", null=True)
	video = EmbedVideoField(null=True, blank=True)
	description = HTMLField(blank=True, verbose_name="Описание")
	d_image = models.ImageField(upload_to='products/', blank=True, verbose_name="Рекламная картинка", null=True)
	d_text = models.CharField(max_length=100, blank=True, verbose_name='Текст на картинке', null=True)
	old_price = models.DecimalField(max_digits=10, decimal_places=0, verbose_name="Старая цена", blank=True, null=True)
	price = models.DecimalField(max_digits=10, decimal_places=0, verbose_name="Цена")
	stock = models.PositiveIntegerField(verbose_name="На складе")
	available = models.BooleanField(default=True, verbose_name="Доступен")
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	pr_wi = models.ManyToManyField('self', blank=True, verbose_name='С этим товаром покупают')
	is_new = models.BooleanField(default=False, verbose_name='Новинка')
	is_hit = models.BooleanField(default=False, verbose_name='Хит')

	def get_absolute_url(self):
		return reverse('shop:ProductDetail', args=[self.category.slug, self.slug + '--' + str(self.id)])

	class Meta:
		ordering = ['name']
		verbose_name = 'Товар'
		verbose_name_plural = 'Товары'
		index_together = [
			['id', 'slug']
		]

	def __unicode__(self):
		return self.name
