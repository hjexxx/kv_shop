# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Cat, Category, Product, InfoModel, InfoModel1, Rent, SliderItem, Review, ContactEmail
from mptt.admin import DraggableMPTTAdmin

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'icon', 'image']
    prepopulated_fields = {'slug': ('name', )}

class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'old_price', 'stock', 'available', 'is_hit','is_new','created']
    list_filter = ['available', 'is_hit','is_new', 'created', 'updated']
    list_editable = ['price', 'old_price','stock', 'available', 'is_hit','is_new' ]
    prepopulated_fields = {'slug': ('name', )}
    search_fields = ['name']
    filter_horizontal = ('pr_wi',)
    # inlines = [ProductInline]


admin.site.register(Cat, DraggableMPTTAdmin, prepopulated_fields = {'slug': ('name', )})
admin.site.register(ContactEmail)
admin.site.register(Product, ProductAdmin)
admin.site.register(InfoModel)
admin.site.register(Rent)
admin.site.register(InfoModel1)
admin.site.register(SliderItem)
admin.site.register(Review)