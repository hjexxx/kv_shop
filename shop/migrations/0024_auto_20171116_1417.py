# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-16 14:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0023_product_pr_wi'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='pr_wi',
        ),
        migrations.AddField(
            model_name='product',
            name='pr_wi',
            field=models.ManyToManyField(related_name='_product_pr_wi_+', to='shop.Product'),
        ),
    ]
