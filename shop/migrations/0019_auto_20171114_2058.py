# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-14 20:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0018_auto_20171112_1850'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='has_child',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='infomodel',
            name='phone',
            field=models.CharField(blank=True, max_length=50, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
        ),
    ]
