# -*- coding: utf-8 -*-
from django import forms
from orders.models import Order

CHOICES = (('1', 'Оплата наличными'), ('2', 'Электронными деньгами (сбербанк онлайн)'))
dCHOICE = (('1', 'Самовывоз'), ('350', 'Доставка по г.Москве — 350 рублей'), ('0', 'Доставка по г.Одинцово — бесплатно (при заказе от 5000 рублей)'), ('370','Доставка по Москвой области — 350 рублей + 20руб/км от МКАД'))
class OrderCreateForm(forms.ModelForm):
	paid_choice = forms.ChoiceField(choices=CHOICES,widget=forms.RadioSelect)
	delivery_choice = forms.ChoiceField(choices=dCHOICE, widget=forms.RadioSelect)
	class Meta:
		model = Order
		fields = ['first_name','delivery_choice','building','last_name', 'email', 'paid_choice', 'phone' ,'street', 'aps',
				  'city']