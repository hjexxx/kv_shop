# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from .models import OrderItem, Order
from decimal import Decimal
from .forms import OrderCreateForm
from cart.cart import Cart
from django.views.generic import TemplateView
from yandex_money.forms import PaymentForm
from yandex_money.models import Payment
from shop.models import InfoModel
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
from . import forms
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
def OrderCreate(request):
	cart = Cart(request)
	if len(cart) > 0:
		if request.method == 'POST':
			form = OrderCreateForm(request.POST)
			if form.is_valid():

				order = form.save()
				request.session['order_id'] = order.id
				for item in cart:
					OrderItem.objects.create(order=order, product=item['product'],
											 price=item['price'],
											 quantity=item['quantity'])
				cart.clear()
				OrderCreated(order.id)
				return render(request, 'orders/order/created.html', {'id': order.id})

	form = OrderCreateForm()
	return render(request, 'orders/order/create.html', {'cart': cart,
														'form': form})

def OrderCreated(order_id):
	order = Order.objects.get(id=order_id)
	delivery = ''
	payment = ''
	itogo = order.get_total_cost() + int(order.delivery_choice)
	for i in forms.dCHOICE:
		if i[0] == str(order.delivery_choice):
			delivery = i[1]
	for i in forms.CHOICES:
		if i[0] == str(order.paid_choice):
			payment = i[1]
	subject, from_email, to = 'Детали заказа #' + str(order.id), settings.DEFAULT_FROM_EMAIL, order.email
	html_content = render_to_string('orders/order_mail.html', {'itogo': itogo,'order': order, 'payment': payment, 'delivery': delivery, 'items': OrderItem.objects.filter(order=order)})
	text_content = strip_tags(html_content)

	msg = EmailMultiAlternatives(subject, text_content, from_email, [to, InfoModel.objects.get(pk=1).email])
	msg.attach_alternative(html_content, "text/html")
	msg.send()


class OrderPage(TemplateView):
	template_name = 'orders/order_page.html'
	
	def get_context_data(self, **kwargs):
		order_id = self.request.session.get('order_id')
		order = get_object_or_404(Order, id=order_id)
		items = OrderItem.objects.filter(order=order)
		payment = Payment(order_amount=order.get_total_cost().quantize(Decimal('.01')))
		payment.save()

		ctx = super(OrderPage, self).get_context_data(**kwargs)
		ctx['form'] = PaymentForm(instance=payment)
		ctx['items'] = items
		return ctx