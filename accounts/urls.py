from django.conf.urls import url, include
from . import views
from django.contrib.auth.views import logout
urlpatterns = [
	url(r'^signup/$', views.signup, name='signup'),
	url(r'^login/$', views.login, name='login'),
	url(r'^logout/$', logout, {'next_page': '/accounts/login/'}),
	url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
		views.activate, name='activate'),
]